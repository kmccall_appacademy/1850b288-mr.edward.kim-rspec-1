def translate(str)
  result = []
  str.split(' ').each do |word|
    result << translate_word(word)
  end
  result.join(' ')
end

def translate_word(word)
  substrings = word.split(/(([aeio]|(?<!q)u).*)/)
  substrings[1] + substrings[0] + 'ay'
end
