def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  arr.inject(0) { |sum, x| sum + x}
end

def multiply(arr)
  arr.inject(:*)
end

def power(num, exp)
  num ** exp
end

def factorial(n)
  n == 0 ? 1 : n * factorial(n-1)
end
