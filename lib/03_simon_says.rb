def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  str + (' ' + str) * (times - 1)
end

def start_of_word(word, length)
  word[0, length]
end

def first_word(str)
  str.split(' ').first
end

def titleize(str)
  title = str.split
  title.each_with_index do |word, index|
    if index.zero? || index == title.length - 1 || word.length > 4
      title[index] = word.capitalize
    end
  end
  title.join(' ')
end
